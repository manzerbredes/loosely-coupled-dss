#!/usr/bin/awk -f

BEGIN {
    RS="\n"
    FS=" "
    CSV_HEADER=""
    CSV_DATA=""
    # ENERGY created below
}

/LOG2PARSE/{
    # First extract what we need
    split($0,fields," ")
    to_parse=fields[8]
    gsub(/\[LOG2PARSE\]\(/,"",to_parse)
    gsub(/\)/,"",to_parse)
    split(to_parse,tokens,"|")

    # Check if we have to build the csv header
    if(CSV_HEADER==""){
        for(i = 1; i<=length(tokens);i++){
            split(tokens[i],h,":")
            if(CSV_HEADER=="")
                CSV_HEADER=h[1]
            else
                CSV_HEADER=CSV_HEADER","h[1]
        }
    }

    # Build a row
    row=""
    for(i = 1; i<=length(tokens);i++){
        split(tokens[i],h,":")
        if(row=="")
            row=h[2]
        else
            row=row","h[2]
    }

    # Add the row to the csv data
    if(CSV_DATA=="")
        CSV_DATA=row
    else
        CSV_DATA=CSV_DATA"\n"row
}


/\[surf_energy\/INFO\] Energy/ {
    $7=substr($7, 1, length($7)-1)
    ENERGY[$7]=$8
}


END {
    print(CSV_HEADER",energy");

    # Print data and add up energy values
    split(CSV_DATA,rows, "\n")
    for(i=1;i<=length(rows);i++){
        split(rows[i],fields, ",")
        print(rows[i]","ENERGY[fields[1]])
    }
}