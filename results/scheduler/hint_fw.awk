#!/usr/bin/awk -f

BEGIN {
    RS="\n"
    FS=" "
    CSV_HEADER="node,ts"
    CSV_DATA=""
    skip=1
    print(CSV_HEADER)
}

/forward a hint successfully/ {
    gsub("]","",$0)
    print($4","$2)
}
