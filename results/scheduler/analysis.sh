#!/usr/bin/env bash

wai=$(dirname $(readlink -f "$0")) # Current script directory

log=$1
out=$2

# Generate csv
cat $log | $wai/wakeup.awk > $wai/wakeup.csv
cat $log | $wai/data.awk > $wai/data.csv
cat $log | $wai/hint.awk > $wai/hint.csv
cat $log | $wai/hint_fw.awk > $wai/hint_fw.csv

cd $wai
Rscript ./wakeup.R &> /dev/null || { echo "Schduler data analysis failed"; exit 1; }
cd - > /dev/null

mv $wai/schedule.png $out