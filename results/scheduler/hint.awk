#!/usr/bin/awk -f

BEGIN {
    RS="\n"
    FS=" "
    CSV_HEADER="node,wakets,duration,rcvat"
    CSV_DATA=""
    skip=1
    print(CSV_HEADER)
}

/add a new hint/ {
    gsub("]","",$0)
    print($4","$10","$15","$2)
}
