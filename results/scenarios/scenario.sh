#!/usr/bin/env bash

set -e

wai=$(dirname $(readlink -f "$0"))
inputs="${wai}/../../inputs.json"
simulator="make -C ${wai}/../../ run"

run-simulation () {
    # Run simulations
    $simulator 2>&1
}


echo "Which scenario to run:"
echo "(a) Baseline"
echo "(b) Extended"
echo "(c) Hint"
echo "(d) Hint+Extended"

read -p "> " -n 1 -r
echo 

case $REPLY in
    a)
        echo "Run baseline scenarios (a)"
        cp ${wai}/baseline.json $inputs
        run-simulation
        ;;
    b)
        echo "Run extended scenarios (b)"
        cp ${wai}/extended.json $inputs
        run-simulation
        ;;
    c)
        echo "Run hint scenarios (c)"
        cp ${wai}/hint.json $inputs
        run-simulation
        ;;
    d)
        echo "Run hint+extended scenarios (d)"
        cp ${wai}/hint_extended.json $inputs
        run-simulation
        ;;
    *)
        echo "Unknown choice"
        exit 1
        ;;
esac



