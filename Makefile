EXEC := simulator
LIBS := ./libs/simgrid/build/lib
INCLUDES := ./libs/simgrid/build/include ./libs/simgrid/include libs/rapidjson/include
SCENARIOS := src/scenarios.cc
CC := g++ $(addprefix -L , $(LIBS)) $(addprefix -I , $(INCLUDES))

all: $(EXEC) $(basename $(notdir $(SCENARIOS)))

$(EXEC): $(filter-out $(SCENARIOS), $(wildcard src/*))
	$(CC) -lsimgrid $^ -o $@

$(basename $(notdir $(SCENARIOS))): $(SCENARIOS)
	$(CC) $^ -o $@

run: $(EXEC)
	export LD_LIBRARY_PATH=$(addprefix :, $(LIBS)) && ./$(EXEC) 10 --cfg=network/bandwidth-factor:1.05 --cfg=network/model:CM02 -–cfg=network/crosstraffic:0

clean:
	-rm $(EXEC) $(basename $(notdir $(SCENARIOS)))