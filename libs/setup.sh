#!/usr/bin/env bash

set -e

[ ! -e "simgrid" ] && git clone https://framagit.org/simgrid/simgrid
[ ! -e "rapidjson"] && git clone https://github.com/Tencent/rapidjson

cd simgrid
mkdir -p build
cd build
cmake ../
make -j4
