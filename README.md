# Impact of loosely coupled data dissemination policies for resource challenged environements

### Setup
- First you need [Boost](https://www.boost.org/) 
- From the project root folder run `cd ./libs && ./setup.sh && cd -`
- And `make`
- If the project compiles successfully, you are ready to run the simulations!
### Paper's Simulations
- To run all the simulations presented in the paper simply run `./results/paper.sh`
this will create the file `results/results.csv` containing all the results. This will also create an
`inputs.json` that you can modify at your convenience
### Simulations
- The file `inputs.json`  controls the inputs to the simulator. You should only care about this file
if you want custom simulations
- To run the simulation just run `make run` This will also generate a file called `platform.xml`  corresponding to the simulated platform
- To generate a *csv* output from the simulation results you can use:
 `make run 2>&1|./parser.awk`
